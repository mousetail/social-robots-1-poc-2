class MainManger {
    constructor() {
        this.order = 2;
        this.data = {
            "Skin": Array.apply(null, {length: 100}).map(()=>10),
            "Temp": Array.apply(null, {length: 100}).map(()=>0),
            "Heart": Array.apply(null, {length: 100}).map(()=>80),
        //    "Oxygen": Array.apply(null, {length: 100}).map(()=>80)
        };

        this.regressionData = {
            "Skin": Array.apply(null, {length: 200}).map(() => 10),
            "Temp": Array.apply(null, {length: 200}).map(() => 0),
            "Heart": Array.apply(null, {length: 200}).map(() => 80),
            "Oxygen": Array.apply(null, {length: 200}).map(()=>80)
        };

        this.units = {
            "Skin": 'dB + SKIN_BASE',
            'Temp': '°C + T_BASE',
            'Heart': 'bpm',
            "Oxygen": "mmHg",
        };

        this.graphs = Object.keys(this.data);

        this.data["Temp"][40]=0.25;

        this.regressionFormula = {
            "Skin": ()=>0,
            "Temp": ()=>0,
            "Heart": ()=>0,
            "Oxygen": ()=>0,
        };

        this.ranges = {
            "Skin": [0, 20],
            "Temp": [-0.75, 0.75],
            "Heart": [0, 220],
            "Oxygen": [50, 150]
        };

        this.safeRanges = {
            "Skin": [-10, 15],
            "Temp": [-1.5, 1.5],
            "Heart": [25, 178.2],
            "Oxygen": [60, 140]
        };

        let keys = Object.keys(this.data);
        for (let i=0; i<keys.length; i++){
            this.getCanvas(keys[i]).addEventListener('mousedown',
                ev => this.clickEventHandler(keys[i], ev)
            );

            this.getCanvas(keys[i]).addEventListener('mousemove',
                ev => this.clickEventHandler(keys[i], ev)
            );
        }

        window.setInterval(this.update.bind(this), 33);
        window.setInterval(this.updateWarnings.bind(this), 333);

        document.getElementById('leftButton').addEventListener('click', this.clickLeft.bind(this));
        document.getElementById('rightButton').addEventListener('click', this.clickRight.bind(this));
        document.getElementById('orderPlusButton').addEventListener('click', this.increaseOrder.bind(this));
        document.getElementById('orderMinusButton').addEventListener('click', this.decreaseOrder.bind(this));
        document.getElementById('zoomInButton').addEventListener('click', this.zoomIn.bind(this));
        document.getElementById('zoomOutButton').addEventListener('click', this.zoomOut.bind(this));
        document.getElementById('dampButton').addEventListener('click', this.toggleDamp.bind(this));
        document.getElementById('thinningButton').addEventListener('click', this.toggleThin.bind(this));
        this.changed = this.graphs.length;
        this.damp = true;
        this.thinning = true;
    }

    toggleThin() {
        this.thinning = ! this.thinning;
        if (this.thinning){
            document.getElementById("thinningButton").innerText = "data thinning enabled";
        }
        else {
            document.getElementById("thinningButton").innerText = "data thinning disabled";
        }
        this.changed += this.graphs.length;
    }

    toggleDamp() {
        this.damp = ! this.damp;
        if (this.damp){
            document.getElementById("dampButton").innerText = "damping enabled";
        }
        else {
            document.getElementById("dampButton").innerText = "damping disabled";
        }
        this.changed += this.graphs.length;
    }

    zoomIn(){
        this.zoom(0.5);
    }

    zoomOut() {
        this.zoom(2);
    }

    zoom(amount) {
        for (let i=0; i<this.graphs.length; i++){
            let range = this.ranges[this.graphs[i]];
            let av = (range[0] + range[1]) / 2;
            range[0] = (range[0] - av)*amount + av;
            range[1] = (range[1] - av)*amount + av;
            this.ranges[this.graphs[i]]=range;
        }
    }

    increaseOrder() {
        this.order++;
        this.changed+=this.graphs.length;
        document.getElementById('orderMinusButton').innerText = "decrease order ("+this.order+")";
        document.getElementById('orderPlusButton').innerText = "increase order ("+this.order+")";
    }

    decreaseOrder() {
        this.order--;
        if (this.order <= 1){
            this.order = 1;
        }
        this.changed+=this.graphs.length;
        document.getElementById('orderMinusButton').innerText = "decrease order ("+this.order+")";
        document.getElementById('orderPlusButton').innerText = "increase order ("+this.order+")";
    }

    clickLeft(ev){
        ev.preventDefault();
        for (let i=0; i<this.graphs.length; i++){
            let g = this.graphs[i];
            let data = this.data[g];

            let len = data.length;
            data = data.concat(this.regressionData[g].slice(data.length, data.length + 10));
            data = data.slice(10, data.length);
            this.data[g]=data;
            if (len !== data.length){
                throw new DOMError("Length changed");
            }

            this.changed+=1;
        }
    }

    clickRight(ev){
        ev.preventDefault();
        for (let i=0; i<this.graphs.length; i++){
            let g = this.graphs[i];
            let data = this.data[g];

            let len = data.length;
            data = Array.apply(null, {length: 10}).map(Number.call, Number).map(n=>this.regressionFormula[g](n-10)[1])
                .concat(data.slice(0, data.length - 10));
            //console.log(data);
            this.data[g]=data;
            if (len !== data.length){
                throw new DOMError("Length changed");
            }

            this.changed+=1;
        }
    }

    clickEventHandler(graph, ev){
        if (!ev.buttons){
            //console.log("ignoring")
            return;
        }
        ev.preventDefault();

        let x = ev.offsetX;
        let y = ev.offsetY;

        let canvas = this.getCanvas(graph);

        if (!(0 <= y <= canvas.clientHeight)){
            throw DOMException("invalid value for y: "+y);
        }

        //console.log([x, y]);
        x = Math.floor(this.regressionData[graph].length * x / canvas.clientWidth);
        y = this.ranges[graph][0] + (this.ranges[graph][1]-this.ranges[graph][0]) * (1-(y / canvas.clientHeight));


        if (x < this.data[graph].length) {
            this.data[graph][x] = y;
        }

        this.changed = this.graphs.length;
    }

    getCanvas(name){
        let container = document.getElementById(name+"Container");
        return container.querySelector('canvas');
    }

    varToCanvas(value, canvas, range) {
        return canvas.height - canvas.height * (value-range[0])/(range[1]-range[0]);
    }

    formatTime(num){
        if (num === 0){
            return "t";
        }
        else if (num >= 0){
            return "t + "+num+"m";
        }
        else {
            return "t - "+(-num)+"m";
        }
    }

    formatNumber(num){
        let t = ''+num;
        let t2 = '';
        let charsDone = 0;
        let gotDecimal = false;
        for (let i=0; i<t.length; i++){
            if (t[i]==='.'){
                if (charsDone >= 1){
                    break;
                }
                else {
                    t2 += '.';
                    gotDecimal = true;
                }
            }
            else if (charsDone < 2 && parseFloat(t[i])>0){
                t2+=t[i];
                charsDone+=1;
            }
            else if (t[i]==='0' && gotDecimal){
                if (charsDone >= 1){
                    break;
                }
                else {
                    t2 += '0';
                }
            }
            else if (t[i]==='0' && !gotDecimal){
                t2+='0';
            }
        }

        if (t2.length === 0 || t2[0]==="."){
            t2 = "0"+t2;
        }

        if (t2[t2.length-1]==="."){
            t2 = t2.slice(0, t2.length-1);
        }

        return t2;
    }

    setBorderStyle(context) {
        context.lineWidth = 4;
        context.strokeStyle = "#888";
    }

    setTickStyle(context) {
        context.lineWidth = 3;
        context.strokeStyle = "#888";
        context.fillStyle = "#888";
    }

    setGridStyle(context) {
        context.lineWidth = 2;
        context.strokeStyle = "#EEEEEE";
    }

    setDataStyle(context) {
        context.strokeStyle = "#000";
        context.lineWidth = 3;
    }

    setTrendStyle(context) {
        context.strokeStyle = "#048";
        context.lineWidth = 2;
    }

    update() {
        for (let i=0; i<this.graphs.length; i++){
            let canvas = this.getCanvas(this.graphs[i]);

            let data = this.data[this.graphs[i]];
            let range = this.ranges[this.graphs[i]];
            let reg = this.regressionData[this.graphs[i]];

            canvas.width = canvas.clientWidth;
            canvas.height = canvas.clientHeight;

            let context = canvas.getContext('2d');

            //draw the scale


            let val = Math.pow(10, Math.floor(Math.log10(range[1]-range[0])));
            if ((range[1] - range[0]) / val < 4){
                val/=2;
            }
            for (let j = val * Math.floor(range[0] / val); j < range[1]; j+=val){
                let y = Math.floor(this.varToCanvas(j, canvas, range));
                this.setGridStyle(context);
                context.beginPath();
                context.moveTo(0, y);
                context.lineTo(canvas.width, y);
                context.stroke();

                this.setTickStyle(context);
                context.beginPath();
                context.moveTo(0, y);
                context.lineTo(10, y);

                context.moveTo(canvas.width - 10, y);
                context.lineTo(canvas.width, y);
                context.stroke();

                let text = ""+this.formatNumber(j)+this.units[this.graphs[i]];
                context.fillText(text, 15, y);
                context.fillText(text, canvas.width - 15 - context.measureText(text).width, y);
            }

            for (let j = 20; j<200; j+=20){

                let x = j * canvas.width / 200;
                this.setGridStyle(context);
                context.beginPath();
                context.moveTo(x, 0);
                context.lineTo(x, canvas.height);
                context.stroke();

                this.setTickStyle(context);
                context.beginPath();

                context.moveTo(x, 0);
                context.lineTo(x, 10);

                context.moveTo(x, canvas.height - 10);
                context.lineTo(x, canvas.height);
                context.stroke();

                let text = this.formatTime(j-100);
                context.fillText(text, x - context.measureText(text).width/2, 25);
                context.fillText(text, x - context.measureText(text).width/2, canvas.height - 15);
            }

            this.setDataStyle(context);

            context.lineWidth = 2;
            context.beginPath();
            for (let j=0; j<data.length; j++){
                context.lineTo(canvas.clientWidth * j / reg.length,
                    this.varToCanvas(data[j], canvas, range)
                );
            }
            context.stroke();

            this.setTrendStyle(context);
            context.beginPath();
            for (let j=0; j<reg.length; j++){
                context.lineTo(canvas.clientWidth * j / reg.length,
                    this.varToCanvas(reg[j], canvas, range)
                );
            }
            context.stroke();


            this.setBorderStyle(context);
            context.strokeRect(0, 0, canvas.width, canvas.height);


            if (this.changed >= 1){
                this.changed -= 1;
                let transformedData = data.map((e, i)=>[i, e]);

                if (this.thinning){
                   transformedData = transformedData.filter(
                       (e, i) => {
                           return (i % 4 === 3) || (i > 25 && i % 4 === 1) || (i > 50 && i % 4 === 3) ||
                               (i > 75 && i % 4 === 0);
                       }
                   );
                }
                //console.log(transformedData);
                let result = regression.polynomial(transformedData , {"order": this.order, "precision": 5});
                //console.log(result);
                this.regressionData[this.graphs[i]] = reg.map((e, j)=>(result.predict(j)[1]));
                if (this.damp) {
                    this.regressionData[this.graphs[i]] =
                        springConstantWrapper(this.regressionData[this.graphs[i]], this.safeRanges[this.graphs[i]][0],
                            this.safeRanges[this.graphs[i]][1], 0.005);
                }
                this.regressionFormula[this.graphs[i]]=result.predict;
                //console.log(result.predict(100));
            }
        }
    }

    updateWarnings() {
        let warn = this.determineWarnings();

        let container = document.getElementById('warningsContainer');

        let markedForDeath = [];
        for (let i=0; i<Math.max(warn.length, container.childNodes.length); i++){
            if (i >= container.childNodes.length){
                container.appendChild(document.createElement('div'));
            }

            if (i >= warn.length){
                markedForDeath.push(container.childNodes[i]);
            }
            else {
                container.childNodes[i].innerText = "["+this.formatTime(warn[i].time - 100)+"]"+warn[i].name+"("+warn[i].severity+")";
            }
        }

        for (let i=0; i<markedForDeath.length; i++){
            container.removeChild(markedForDeath[i]);
        }
    }

    determineWarnings() {
        let warn = [];
        for (let time = 0; time < 200; time++){
            let dat = {};
            if (time < 100){
                for (let i=0; i<this.graphs.length; i++) {
                    dat[this.graphs[i]] = this.data[this.graphs[i]][time];
                    if (time > 4) {
                        dat[this.graphs[i] + "delta"] =
                            this.data[this.graphs[i]][time] * 0.25 -
                            this.data[this.graphs[i]][time - 4] * 0.25;
                    }
                    else {
                        dat[this.graphs[i]+"delta"] = 0;
                    }
                }
            }
            else {
                for (let i=0; i<this.graphs.length; i++) {
                    dat[this.graphs[i]] = this.regressionData[this.graphs[i]][time];
                    dat[this.graphs[i]+"delta"] =
                        this.regressionData[this.graphs[i]][time] -
                        this.regressionData[this.graphs[i]][time - 1];
                }
            }

            warn= warn.concat(Warnings.determineWarnings(time, dat));
        }

        let finalWarn = [];
        /*if (warn.length >= 1) {
            console.log(warn);
        }*/
        let g2 = this.graphs.concat(["Combi"]);
        for (let i=0; i<g2.length; i++){
            let filteredWarn = warn.filter(x=>x.category === g2[i]);
            if (filteredWarn.length >= 1) {
                //console.log(filteredWarn);
                filteredWarn.sort((a, b) => (a.severity === b.severity ? a.time - b.time : b.severity - a.severity));

                finalWarn.push(filteredWarn[0]);
            }
        }
        return finalWarn;
    }
}