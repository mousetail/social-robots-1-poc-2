class Warnings {
    static determineWarnings(time, dat){
        return Warnings.determineTemperatureWarnings(time, dat["Temp"]).
        concat(
            Warnings.determineHeartrateWarnings(time, dat["Heart"],
                dat["Heartdelta"], 20)
        ).concat(
            Warnings.determineSkinConductanceWarnings(time, dat["Skin"])
        ).concat(
            Warnings.determineCombinedWarnings(time,
                dat["Temp"],
                dat["Tempdelta"],
                dat["Heart"],
                dat["Heartdelta"],
                dat["Skin"],
                dat["Skindelta"])
        );
    }

    static determineTemperatureWarnings(time, temp){
        if (-0.5 <= temp && temp <= 0.5){
            return [];
        }
        else if (-1 <= temp && temp <= -0.5){
            return [new Warning(time, "Dropping body temperature", "Temp", 0)]
        }
        else if (-2 <= temp && temp <= -1){
            return [new Warning(time, "Mild hypothermia", "Temp", 1)];
        }
        else if (-3 <= temp && temp <= -2){
            return [new Warning(time, "Severe hypothermia", "Temp", 2)];
        }
        else if (0.5 <= temp && temp <= 1.5){
            return [new Warning(time, "Mild hyperthermia", "Temp", 1)];
        }
        else if (1.5 <= temp && temp <= 3){
            return [new Warning(time, "Severe hyperthermia", "Temp", 2)];
        }
        else {
            return [];
        }
    }

    static determineHeartrateWarnings(time, heartRate, deltaHeartRate, age){

        let w = [];

        let HRMax = 211 - (0.64 * age);
        if (heartRate > HRMax){
           w.push(new Warning(time, "Abnormally High Heartrate", "Heart", 1));
        }
        else if (heartRate < 50){
            w.push(new Warning(time, "Abnormally Low Heartrate", "Heart", 2));
        }

        return w;
    }

    static determineSkinConductanceWarnings(time, skin){
        /*if (skin < -10){
            return [new Warning(time, "Dehydration", "Skin", 1)];
        }
        else {
            return [];
        }*/
        return [];
    }

    static determineCombinedWarnings(time,
                                     t, t_d,
                                     h, h_d,
                                     s, s_d
    ){
        t_d *= 2;
        let w_t0, w_td1, w_ti1, w_td2, w_ti2 = false;
        if (-0.0003333<t_d && t_d < 1.000333){
            w_t0 = true;
        }
        else if (-0.0005<t_d){
            w_td1 = true;
        }
        else if (0.000333<t_d && t_d <0.000666){
            w_ti1 = true;
        }
        else if (t_d < -0.0005){
            w_td2 = true;
        }
        else if (t_d > 0){
            w_ti2 = true;
        }

        let w_hr0, w_hr1, w_hr2, w_hrd;
        if (10 <= h_d && h_d <= 10){
            w_hr0 = true;
        }
        else if (11 < h_d && h_d <= 12){
            w_hr1 = true;
        }
        else if (h_d > 12){
            w_hr2 = true;
        }
        else if (h_d < 0){
            w_hrd = true;
        }

        if (w_hr2 && s_d > 0 && s > 0 && (w_ti1 || w_t0)){
            return [];
        }
        else if ((w_hr1 || w_hr0) && (s_d !== 0) && s > 0){
            return [new Warning(time, "Temperature increasing slightly too fast, consider resting",
                "Combi", 0)];
        }
        else if (w_hr1 && s_d > 0 && c > 0 && w_td1){
            return [new Warning(time, "Unknown warning", "Combi", 0)];
        }
        else if (w_hr2 && s_d!==0 && s > 0 && (w_ti1 || w_ti2)){
            return [new Warning(time, "Over Exhausition", "Combi", 1)];
        }
        else if (w_hr2 && s_d>0 && s > 0 && (w_td1 || w_td2)){
            return [new Warning(time, "Unknown warning", "Combi", 1)];
        }
        else if (w_hr2 && s_d < 0 && s > 0 && (w_td1 || w_td2)){
            return [new Warning(time, "Simultainious exhaustion, undercooling, and dehydration", "Combi", 1)]
        }
        else if (w_hr1 && s_d > 0 && s > 0 && w_td2){
            return [new Warning(time, "Hypothermia", "Combi", 1)];
        }
        else if (s < 0){
            return [new Warning(time, "Dehydration", "Combi", 1)];
        }

        return [];
    }
}

console.log(Warnings);

class Warning {
    constructor(time, name, category, severity) {
        this.time = time;
        this.name = name;
        this.category = category;
        this.severity = severity;
    }
}